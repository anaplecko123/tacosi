package com.praktikum.taco.model;

public class Predmet {
    private Artikel artikel;
    private int kolicina;

    public Predmet(Artikel artikel, int kolicina) {
        this.artikel = artikel;
        this.kolicina = kolicina;
    }

    public Artikel getArtikel() {
        return artikel;
    }

    public void setArtikel(Artikel artikel) {
        this.artikel = artikel;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }
}
