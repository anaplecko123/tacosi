package com.praktikum.taco.model;

import ch.qos.logback.classic.boolex.GEventEvaluator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class PrikazNarocil implements Serializable {
    int id;
    int idUporabnik;
    String datum;
    String naslov;
    String kraj;
    String opis;
    double cena;
     //konstruktor

    public PrikazNarocil() {
    }


    // param. konstruktor
    public PrikazNarocil(int id, String datum, String naslov, String kraj, String opis, double cena,int idUporabnik){
        this.id=id;
        this.datum=datum;
        this.kraj=kraj;
        this.naslov=naslov;
        this.cena=cena;
        this.opis=opis;
        this.idUporabnik=idUporabnik;
    }

    // get in set metode

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getOpis() {
        return opis;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public String getKraj() {
        return kraj;
    }

    public int getIdUporabnik() {
        return idUporabnik;
    }

    public void setIdUporabnik(int idUporabnik) {
        this.idUporabnik = idUporabnik;
    }
}
